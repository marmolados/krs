// KRS required includes
#include "cfg.h"

// KRS definitions
#define FastPrintFormat(errorCode) { FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM, \
					NULL, \
					errorCode, \
					0, \
					&lpError, \
					0, \
					NULL); \
				if (!lpError) \
					return; \
				if (!WriteConsoleA(hStd, lpError, lstrlenA(lpError), &nNumberOfCharsWritten, NULL)) \
					return; \
				LocalFree(lpError); \
				lpError = NULL; }

#define FastPrint(msg) WriteConsoleA(hStd, msg, lstrlenA(msg), &nNumberOfCharsWritten, NULL)

#define MakeFile(disposition) CreateFileA(lpFilePath, GENERIC_WRITE, FILE_SHARE_READ, NULL, disposition, FILE_ATTRIBUTE_NORMAL, NULL)

#ifndef TAG_VER
#define TAG_VER "1.0.0.0"
#endif

#define PROGRAM_INFO ("KeyRegSearcher by Marmolados [" TAG_VER "]\n" \
	"Copyright (c) " __DATE__ " NoN-Profit Yet Software, Inc. All rights reserved.\n\n")

#define PROGRAM_NAME "krs"
#define PROGRAM_HELP "Usage:\n\t" PROGRAM_NAME " <main key index> <word to search> [<optional result filename>]\n" \
	"Example:\n\t" PROGRAM_NAME " 1 Windows winregs.log\n\n" \
	"Legend:\n\t[Main registry keys (default: HKLM)]" \
		"\n\t\t1  - HKCR   (HKEY_CLASSES_ROOT)" \
		"\n\t\t2  - HKCU   (HKEY_CURRENT_USER)" \
		"\n\t\t3  - HKLM   (HKEY_LOCAL_MACHINE)" \
		"\n\t\t4  - HKU    (HKEY_USERS)" \
		"\n\t\t5  - HKPD   (HKEY_PERFORMANCE_DATA)" \
		"\n\t\t6  - HKPT   (HKEY_PERFORMANCE_TEXT)" \
		"\n\t\t7  - HKPN   (HKEY_PERFORMANCE_NLSTEXT)" \
		"\n\t\t8  - HKCC   (HKEY_CURRENT_CONFIG)" \
		"\n\t\t9  - HKDD   (HKEY_DYN_DATA)" \
		"\n\t\t10 - HKCULS (HKEY_CURRENT_USER_LOCAL_SETTINGS)\n"

#define DONE_MSG "Done!\n"
#define START_MSG "Searching for keys... "
#define RESULT_MSG DONE_MSG "Result storing... "

#define PRINT_PARAM 'p'