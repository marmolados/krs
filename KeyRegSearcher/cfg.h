// configure includes
#include <Windows.h>
#include <WindowsX.h>
#include <Shlwapi.h>

// configure required libraries
#pragma comment(lib,"Kernel32.lib")
#pragma comment(lib,"Shlwapi.lib")
#pragma comment(lib,"Shell32.lib")
#pragma comment(lib,"Advapi32.lib")

// configure definitions
#define MAX_INDEX_COUNT 2048
#define MAX_KEY_INDEX_COUNT (MAX_INDEX_COUNT * 256)

// configure entry point
#define ENTRY_POINT (KRSEntryPoint)

// configure preprocesor definitions
#ifndef WIN32
#define WIN32
#else
#ifndef _WIN64
#define _WIN64
#endif
#endif
