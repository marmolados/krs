#include "krs.h"

// KRS entry point function
void KRSEntryPoint(void) {
	HANDLE hStd = GetStdHandle(STD_OUTPUT_HANDLE);
	if (!hStd || (hStd == INVALID_HANDLE_VALUE))
		return; // this is not acceptable!
	HKEY hKey = HKEY_LOCAL_MACHINE, hkResult = 0;
	CHAR lpBuffer[MAX_INDEX_COUNT], lpBuffer2[MAX_INDEX_COUNT], lpBuffer3[MAX_INDEX_COUNT], lpFilePath[MAX_PATH];
	SecureZeroMemory(lpBuffer, sizeof(lpBuffer));
	SecureZeroMemory(lpBuffer2, sizeof(lpBuffer2));
	SecureZeroMemory(lpBuffer3, sizeof(lpBuffer3));
	SecureZeroMemory(lpFilePath, sizeof(lpFilePath));
	DWORD cchName = sizeof(lpBuffer), cchClass = sizeof(lpBuffer2), nNumberOfCharsWritten = 0, ulcSubKeys = 0,
		dwMainIndex = 0, dwMainIndexPosition = 0, dwTreeIndex = 0, *dwIndexTree = LocalAlloc(LPTR, MAX_INDEX_COUNT * sizeof(*dwIndexTree));
	lstrcpyA(lpBuffer, PROGRAM_INFO);
	FastPrint(lpBuffer);
	LPSTR lpSubKey = &lpBuffer3, lpName = &lpBuffer, lpClass = &lpBuffer2,
		*lpKeyList = LocalAlloc(LPTR, MAX_KEY_INDEX_COUNT * sizeof(*lpKeyList)),
		lpEnd = NULL, lpLastEnd = NULL, lpWord = NULL, lpError = NULL;
	if (!dwIndexTree) {
		FastPrintFormat(GetLastError());
		return;
	}
	if (!lpKeyList) {
		FastPrintFormat(GetLastError());
		return;
	}

	int nNumArgs, nKeyId;
	LPCWSTR lpCmdLine = GetCommandLineW();
	LPWSTR * lpCmdArgs = CommandLineToArgvW(lpCmdLine, &nNumArgs), lpCmdArg = *(++lpCmdArgs);
	LSTATUS lStatus;
	if (nNumArgs < 3) {
		FastPrint(PROGRAM_HELP);
		goto __freeArgs;
	}
	else {
		nKeyId = StrToIntW(lpCmdArg);
		switch (nKeyId)
		{
		case 1: (hKey = HKEY_CLASSES_ROOT); break;
		case 2: (hKey = HKEY_CURRENT_USER); break;
		default: case 3: (hKey = HKEY_LOCAL_MACHINE); break;
		case 4: (hKey = HKEY_USERS); break;
		case 5: (hKey = HKEY_PERFORMANCE_DATA); break;
		case 6: (hKey = HKEY_PERFORMANCE_TEXT); break;
		case 7: (hKey = HKEY_PERFORMANCE_NLSTEXT); break;
		case 8: (hKey = HKEY_CURRENT_CONFIG); break;
		case 9: (hKey = HKEY_DYN_DATA); break;
		case 10: (hKey = HKEY_CURRENT_USER_LOCAL_SETTINGS); break;
		}
		if (!lpWord)
			lpWord = LocalAlloc(LPTR, MAX_PATH);
		lpCmdArg = *(lpCmdArgs + 1);
		WideCharToMultiByte(CP_UTF8, 0, lpCmdArg, lstrlenW(lpCmdArg), lpWord, MAX_PATH, NULL, FALSE);
		if((lStatus = GetLastError()) != ERROR_SUCCESS){
			FastPrintFormat(lStatus);
			nNumArgs = 0;
		}
		if (nNumArgs > 3) {
			lpCmdArg = *(lpCmdArgs + 2);
			WideCharToMultiByte(CP_UTF8, 0, lpCmdArg, lstrlenW(lpCmdArg), lpFilePath, MAX_PATH, NULL, FALSE);
			if ((lStatus = GetLastError()) != ERROR_SUCCESS)
				FastPrintFormat(lStatus);
		}
	}
__freeArgs:
	if (nNumArgs < 3) {
		LocalFree(lpCmdArgs);
		return;
	}

	if(!lpFilePath[0])
		lstrcpyA(lpFilePath, "regtrace.log");

	FILETIME ftLastWriteTime;
	lStatus = RegOpenKeyExA(hKey, NULL, 0, KEY_ENUMERATE_SUB_KEYS, &hkResult);
	if (lStatus != ERROR_SUCCESS) {
		FastPrintFormat(lStatus);
		return;
	}

	lStatus = RegQueryInfoKeyA(hKey, lpClass, &cchClass, NULL, &ulcSubKeys, NULL, NULL, NULL, NULL, NULL, NULL, &ftLastWriteTime);
	if (lStatus != ERROR_SUCCESS) {
		FastPrintFormat(lStatus);
		return;
	}

	FastPrint(START_MSG);

	// main Key searcher loop
	do {
		lStatus = RegOpenKeyExA(hKey, lpSubKey, 0, KEY_ENUMERATE_SUB_KEYS, &hkResult);
		if (lStatus != ERROR_SUCCESS) // if error is here we go to enum validation do what it should be done!
			goto __statusError;
		cchName = sizeof(lpBuffer);
		cchClass = sizeof(lpBuffer2);
		lStatus = RegEnumKeyExA(hkResult, dwIndexTree[dwMainIndexPosition], lpName, &cchName, NULL, lpClass, &cchClass, &ftLastWriteTime);
		if (lStatus != ERROR_SUCCESS) {
		__statusError:
			*lpName = TEXT('\\');
			*(lpName + 1) = TEXT('\0');
			lpClass = lpSubKey;
			lpLastEnd = NULL;
			do {
				lpEnd = StrStrA(lpSubKey, lpName);
				lpSubKey = lpEnd + 1;
				if (lpEnd)
					lpLastEnd = lpEnd;
			} while (lpEnd);

			lpSubKey = lpClass;

			if (!lpLastEnd)
				*lpSubKey = TEXT('\0');
			else
				*lpLastEnd = TEXT('\0');

			lpClass = lpBuffer2;
			if (dwMainIndexPosition)
				dwIndexTree[dwMainIndexPosition] = 0;
			dwIndexTree[(!dwMainIndexPosition ? 0 : --dwMainIndexPosition)]++;
		}
		else {
			lpEnd = lpSubKey + lstrlenA(lpSubKey);

			if ((*(lpEnd - 1) != TEXT('\\')) && (lpEnd != lpSubKey))
			{
				*lpEnd = TEXT('\\');
				lpEnd++;
				*lpEnd = TEXT('\0');
			}

			*lpEnd = TEXT('\0');
			lstrcatA(lpSubKey, lpName);

			if (!lpKeyList[dwTreeIndex])
				lpKeyList[dwTreeIndex] = LocalAlloc(LPTR, (nNumberOfCharsWritten = lstrlenA(lpSubKey)) + 1);
			if (lpKeyList[dwTreeIndex])
				lstrcpyA(lpKeyList[dwTreeIndex], lpSubKey);
			else
				break;

			dwMainIndexPosition++;
			dwTreeIndex++;
		}
		RegCloseKey(hKey);
	} while ((dwTreeIndex < MAX_KEY_INDEX_COUNT) && (dwIndexTree[0] < ulcSubKeys));

	FastPrint(RESULT_MSG);

	// after job done store result
	HANDLE hFileHandle = INVALID_HANDLE_VALUE;
	OVERLAPPED ol;
	hFileHandle = MakeFile(CREATE_NEW);
	if (hFileHandle == INVALID_HANDLE_VALUE)
		hFileHandle = MakeFile(TRUNCATE_EXISTING);
	if (hFileHandle == INVALID_HANDLE_VALUE)
		hFileHandle = MakeFile(CREATE_ALWAYS);
	if (hFileHandle != INVALID_HANDLE_VALUE) {
		*lpName = TEXT('\0');
		SecureZeroMemory(&ol, sizeof(ol));
		*lpClass = TEXT('\n');
		*(lpClass + 1) = TEXT('\0');
	}
	lStatus = GetLastError();

	for (register int i = 0; i < MAX_KEY_INDEX_COUNT; i++)
		if (lpKeyList[i]) {
			if (hFileHandle != INVALID_HANDLE_VALUE) {
				if (!StrStrA(lpKeyList[i], lpWord))
					goto __skipWrite;

				lstrcpyA(lpName, lpKeyList[i]);
				lstrcatA(lpName, lpClass);

				if((nNumArgs > 4) && (*(lpCmdArgs + 3) == PRINT_PARAM))
					FastPrint(lpName);

				if (!WriteFile(hFileHandle, lpName, (cchName = lstrlenA(lpName)), &nNumberOfCharsWritten, &ol))
					lStatus = GetLastError();
				if (((DWORD)(ol.Offset + cchName)) < ol.Offset) // the same thing as we'll check it like this ((QWORD)(ol.Offset + cchName) > MAXDWORD)
					ol.OffsetHigh++;
				ol.Offset += cchName;
			}
		__skipWrite:
			LocalFree(lpKeyList[i]);
		}
		else
			break;

	if ((hFileHandle != INVALID_HANDLE_VALUE) && (lStatus == ERROR_SUCCESS))
		FastPrint(DONE_MSG);
	else
		FastPrintFormat(lStatus);

	if (hFileHandle != INVALID_HANDLE_VALUE)
		CloseHandle(hFileHandle);

	LocalFree(lpCmdArgs);

	if (lpKeyList)
		LocalFree(lpKeyList);

	if (lpWord)
		LocalFree(lpWord);
	
	if (dwIndexTree)
		LocalFree(dwIndexTree);
}